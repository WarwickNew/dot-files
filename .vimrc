set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'git://git.wincent.com/command-t.git'
Plugin 'rstacruz/sparkup'
Plugin 'L9'

" theme
Plugin 'morhetz/gruvbox'
"Plugin 'ryanoasis/vim-devicons'

" latex
Plugin 'xuhdev/vim-latex-live-preview'

" coding essentials
Plugin 'ycm-core/YouCompleteMe'
Plugin 'scrooloose/nerdtree'
Plugin 'sheerun/vim-polyglot'
"" js essentials
Plugin 'dense-analysis/ale'

" Vim org-mode
Plugin 'jceb/vim-orgmode'
Plugin 'utl.vim'
Plugin 'taglist.vim'
Plugin 'repeat.vim'
Plugin 'Tagbar'
Plugin 'speeddating.vim'
Plugin 'NrrwRgn'
Plugin 'mattn/calendar-vim'
Plugin 'SyntaxRange'

" Syntax
Plugin 'ShowTrailingWhitespace'
Plugin 'Yggdroot/indentLine'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


" An example for a vimrc file.
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2017 Sep 20
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Get the defaults that most users want.
if !has("nvim")
  source $VIMRUNTIME/defaults.vim
endif

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  if has('persistent_undo')
    set undofile	" keep an undo file (undo changes after closing)
  endif
endif

if &t_Co > 2 || has("gui_running")
  " Switch on highlighting the last used search pattern.
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
    au!

    " For all text files set 'textwidth' to 80 characters and enable spell
    " check
    autocmd FileType org,tex,text setlocal textwidth=79
    autocmd FileType org,tex,text setlocal spell spelllang=en_gb
    autocmd BufNew * match OverLength /\%121v.\+/
    " Transparancy on neovim
    autocmd VimEnter * hi! Normal guibg=NONE

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Add optional packages.
"
" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
" The ! means the package won't be loaded right away but when plugins are
" loaded during initialization.
if has('syntax') && has('eval') && !has('nvim')
  packadd! matchit
endif

"" User definitions
" Make code more readable
set breakindent
set background=dark
colorscheme gruvbox "desert
set cursorline
highlight CursorLine term=bold cterm=bold guibg=#191919
set number
" Transparent
if has("termguicolors")     " set true colors
  set t_8f=[38;2;%lu;%lu;%lum
  set t_8b=[48;2;%lu;%lu;%lum
  set termguicolors
endif
highlight ALEError ctermbg=darkgrey cterm=underline
highlight ALEWarning ctermbg=darkgrey cterm=underline
" Keep all the lovely backup and undo stuff but hide it in a directory
"set dir=~/.cache/vim, " TODO make a cron job to clean this out
set backupdir=~/.cache/vim, " TODO make a cron job to clean this out
set undodir=~/.cache/vim, " TODO make a cron job to clean this out
set backup
set undofile

" Sensible line length
"set colorcolumn=81
"highlight ColorColumn ctermbg=darkgray
highlight OverLength ctermbg=darkgrey guibg=#592929
let &colorcolumn="81"
" File navigator
map <C-n> :NERDTreeToggle<CR>

" Tab handling
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
let g:indentLine_char = '▏'

" hide test search when I look elsewhere
"nnoremap <esc> :noh<return><esc>
"stopinsert

" allow project specific vimrc
set exrc
set secure

" folding settings
set foldmethod=syntax
set foldnestmax=10
set nofoldenable

" Please finally learn touch typing youuseless programmer
"noremap ; l
"noremap l k
"noremap k j
"noremap j h
" TODO build system, launch system, path variables
